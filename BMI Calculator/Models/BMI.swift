//
//  BMI.swift
//  BMI Calculator
//
//  Created by Jonny Porter on 1/16/20.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import UIKit

struct BMI {
	var value: Float
	var advice: String
	var color: UIColor
	
	}
