//
//  Calc.swift
//  BMI Calculator
//
//  Created by Jonny Porter on 1/15/20.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import Foundation
import UIKit

struct Calc {
	var bmi: BMI?
    var weight: Float = 0
    var height: Float = 0
    
    func exponentCalc(_ unit: Float) -> Float {
        return unit * unit
    }
    
    func usCalc(_ pounds: Float, _ inches: Float) -> Float {
		let bmiValue = 703 * pounds / exponentCalc(inches)
        return bmiValue
    }
    
	mutating func getGlobalCalc(_ kilograms: Float, _ meters: Float) {
		let bmiValue = kilograms / exponentCalc(meters)
			if bmiValue > 24.9 {
				bmi = BMI(value: bmiValue, advice: "Looks like you could lose some weight.", color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1))
			} else if bmiValue <= 24.9 && bmiValue > 18.5 {
				bmi = BMI(value: bmiValue, advice: "You'll want to maintain this weight.", color: #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1))
			} else {
				bmi = BMI(value: bmiValue, advice: "Looks like you could gain some weight.", color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
			}
    }
	
	func getBMIColor() -> UIColor {
		return bmi?.color ?? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
	}
	
	func getBMIAdvice() -> String {
		return bmi?.advice ?? ""
	}
    
	func getBMIValue() -> String {
		let bmiRounded = String(format: "%.1f", bmi?.value ?? 0)
		return bmiRounded
    }
	
	func roundValue(_ value: Float) -> String {
		return String(format: "%.1f", value)
	}
    
    
}
