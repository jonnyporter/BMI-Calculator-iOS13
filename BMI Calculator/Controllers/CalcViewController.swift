//
//  ViewController.swift
//  BMI Calculator
//
//  Created by Angela Yu on 21/08/2019.
//  Copyright © 2019 Angela Yu. All rights reserved.
//

import UIKit

class CalcViewController: UIViewController {
	
    var calc = Calc()
	let resultVC = ResultViewController()
	
    @IBOutlet weak var globalHeightLabel: UILabel!
    @IBOutlet weak var globalWeightLabel: UILabel!
	@IBOutlet weak var heightSliderValue: UISlider!
	@IBOutlet weak var weightSliderValue: UISlider!
	
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
	
    func updateUI() {
		calc.height = heightSliderValue.value
		calc.weight = weightSliderValue.value
		let height = calc.roundValue(calc.height)
		let weight = Int(calc.weight)
        globalHeightLabel.text = "\(height)m"
        globalWeightLabel.text = "\(weight)Kg"

    }
    
    @IBAction func heightSliderChanged(_ sender: UISlider) {
        updateUI()
    }
    
    @IBAction func weightSliderChanged(_ sender: UISlider) {
        updateUI()
    }
	
    @IBAction func calcButton(_ sender: UIButton) {
		calc.getGlobalCalc(calc.weight, calc.height)
		performSegue(withIdentifier: "goToResult", sender: self)
    }

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "goToResult" {
			let destinationVC = segue.destination as! ResultViewController
			updateUI()
			destinationVC.bmiValue = calc.getBMIValue()
			destinationVC.bmiAdvice = calc.getBMIAdvice()
			destinationVC.bmiColor = calc.getBMIColor()
		}
	}
}

